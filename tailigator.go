package main

import (
	"bufio"
	"fmt"
	"github.com/logrusorgru/aurora"
	"github.com/nsqio/go-nsq"
	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"time"
)

var (
	wg = sync.WaitGroup{}
	fileList = sync.Map{}
	flist = make(map[string]bool)
	logs = []string{}
	lock = sync.Mutex{}
	length = 0
	limit = 500000
	config *Config
	ok bool

)

type Config struct {
	SearchDir string `yaml: "searchdir"`
	Nsq string `yaml: "nsq"`
	Topic string `yaml: "topic"`
	MaxSize int64	`yaml: "maxsize"`
	Truncate bool	`yaml: "truncate"`
}

func ReadConfigfile(configfile string) (*Config, bool) {
	cfgdata, err := ioutil.ReadFile(configfile)

	if err != nil {
		log.Panic("Cannot open config file from " + configfile)
	}

	t := Config{}

	err = yaml.Unmarshal([]byte(cfgdata), &t)

	if err != nil {
		log.Panic("Cannot map yml config file to interface, possible syntax error")
		log.Panic(err)
	}

	return &t, true
}


func tailIt(f string) {
	/*
	t, err := tail.TailFile(f, tail.Config{Follow: true})
	if err != nil {
		log.Error(err)
	}
	
	for line := range t.Lines {
		lock.Lock()
		logs = append(logs, line.Text)
		lock.Unlock()
	}
	*/
	i := 0
	file, err := os.Open(f)
	if err != nil {
		log.Error(err)
		return
	}
	fstat, _ := file.Stat()
	reader := bufio.NewReader(file)
	if err != nil {
		log.Fatal(err)
	}
	notExist := 0
	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				time.Sleep(1 * time.Second)
				if fstat2, err := os.Stat(f); err == nil {
					if !os.SameFile(fstat, fstat2) {
						log.Warn(aurora.Red("File changed"))
						file.Close()
						file, err = os.Open(f)
						if err != nil {
							log.Fatal(err)
						}
						fstat, _ = file.Stat()
						reader = bufio.NewReader(file)
					}
				}
			}
			if _, err := os.Stat(f); os.IsNotExist(err) {
				notExist++
				if notExist > 10 {
					break
				}
				log.Warnf("%d: Waiting 2 seconds, to check if file returns", aurora.Red(notExist))
				time.Sleep(2 * time.Second)
			}
		} else {
			for length >= limit {
				time.Sleep(5 * time.Second)
			}
			lock.Lock()
			logs = append(logs, fmt.Sprintf("%s %s", line, f))
			fileList.Store(f, i)
			lock.Unlock()
			i++
		}
	}
}

func readBackup(f string) {
	file, err := os.Open(f)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	reader := bufio.NewReader(file)
	i := 0
	lock.Lock()
	startint, _ := fileList.Load(f)
	start, _ := strconv.Atoi(fmt.Sprintf("%d", startint))
	lock.Unlock()
	for {
		if i > start {
			line, err := reader.ReadString('\n')
			if err != nil {
				break
			}
			for length >= limit {
				time.Sleep(10 * time.Second)
			}
			lock.Lock()
			logs = append(logs, fmt.Sprintf("%s %s", line, f))
			lock.Unlock()
		}
		i++
	}
	
	os.Remove(f)
}

func sendIt() {
	conf := 	nsq.NewConfig()
	w, _ := nsq.NewProducer(config.Nsq, conf)
	defer w.Stop()
	
	for {
		//lock.Lock()
		length = len(logs)
		//lock.Unlock()
		if length == 0 {
			time.Sleep(5 * time.Second)
		} 

		if length > 0 {
			/*
			if length >= 100000 {

				lock.Lock()
				l2send = logs[:100000]
				logs = logs[100000:]
				lock.Unlock()
			} else {
			*/
			//lock.Lock()
			bs := []byte(logs[0])
			logs = logs[1:]

			//lock.Unlock()
			//}
			if len(bs) != 0 {
				//log.Info(aurora.Green(fmt.Sprintf("%d", length)))
				err := w.Publish(config.Topic, bs)

				if err != nil {
					log.Panic("could not connect")
				}
			}
		}
	}
}

func getFileSize(f string){
	defer wg.Done()
	
	for {
		fi, err := os.Stat(f)
		if err != nil {
			log.Error(aurora.Red(err))
			break
		}

		if fi.Size() > 1048576 * config.MaxSize && ! strings.Contains(f, "backup_") {
			log.Infof("%s is bigger than %dMB", f, config.MaxSize)
			s := strings.Split(f, "/")
			newf := fmt.Sprintf("%s/backup_%s", config.SearchDir, s[len(s)-1])
			if !config.Truncate {
				os.Rename(f, newf)
			} else {
				source, err := os.Open(f)
				if err != nil {
					log.Error(err)
				}

				dest, err := os.Create(newf)
				if err != nil {
					log.Error(err)
				}
				_, err = io.Copy(dest, source)
				if err != nil {
					log.Error(err)
				}

				log.Infof("truncating %s", f)
				os.Truncate(f, 0)
			}
			//lock.Lock()
			sval, _ := fileList.Load(f)
			fileList.Store(newf, sval)
			fileList.Store(f, 0)
			//lock.Unlock()
			go readBackup(newf)
		} else {
			log.Infof("%s is %d bytes long", f, fi.Size())
		}
		time.Sleep(5 * time.Second)
	}
}

func ifContains(f string) bool {
	for x := range flist {
		if x == f {
			return true
		}
	}
	return false
}

func failOnError(err error, msg string) {
	if err != nil {
		log.Fatalf("%s: %s", msg, err)
	}
}

func checkFile() {
	for x := range flist {
		if _, err := os.Stat(x); os.IsNotExist(err) {
			lock.Lock()
			delete(flist, x)
			lock.Unlock()
			fileList.Delete(x)
		}
	}
}

func main() {
	customFormatter := new(log.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	customFormatter.FullTimestamp = true
	customFormatter.ForceColors = true
	log.SetFormatter(customFormatter)
	log.SetOutput(os.Stdout)

	config, ok = ReadConfigfile("config.yml")

	if !ok {
		log.Error("There was something wrong in the configfile!")
	}

	go sendIt()
	
	for {
		err := filepath.Walk(config.SearchDir, func(path string, f os.FileInfo, err error) error {
			if strings.HasSuffix(path, ".log") && !ifContains(path) {
				log.Infof("added %s", path)
				lock.Lock()
				//fileList[path] = 0
				flist[path] = true
				lock.Unlock()
				fileList.Store(path, 0)
				wg.Add(1)
				go getFileSize(path)
				if !strings.Contains(path, "backup_") {
					go tailIt(path)
				}
			}
			return nil
		})

		if err != nil {
			log.Fatal(err)
		}
		
		checkFile()
		time.Sleep(5 * time.Second)
	}
	wg.Wait()

	/*
	for _, file := range fileList {
		fmt.Println(file)
	}
	*/

}
