FROM ubuntu:18.04

COPY files/* /
RUN chmod +x tailigator

CMD ["./tailigator"]